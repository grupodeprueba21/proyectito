## Sitio web estático

## Crear Ramas principales

1. Crea la rama develop y se mueve hacia ella

  ```sh
  git checkout -b develop
  ```
2. Vamos subir la rama develop a gitlab
  ```sh
   git push -u origin develop
  ```
3. Creamos el archivo de ci/cd (.gitlab-ci.yml)
 ```sh
 git add .gitlab-ci.yml
 git commit -m "Agrega el archivo .gitlab-ci.yml para la configuración de CI/CD"
 git push origin develop
 ```

